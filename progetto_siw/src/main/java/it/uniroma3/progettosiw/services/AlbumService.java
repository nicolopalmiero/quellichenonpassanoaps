package it.uniroma3.progettosiw.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.progettosiw.model.Album;
import it.uniroma3.progettosiw.repository.AlbumRepository;

@Service
public class AlbumService {

	@Autowired
	private AlbumRepository albumRepository;

	@Transactional
	public Album inserisci(Album album)
	{
		return this.albumRepository.save(album);
	}
	
	@Transactional
	public List<Album> albumPerNome(String nome)
	{
		return this.albumRepository.findByName(nome);
	}
	
	@Transactional
	public List<Album> tutti()
	{
		return (List<Album>) this.albumRepository.findAll();
	}

	public Album albumPerId(Long id) {
		return this.albumRepository.findById(id).get();
	}
}
