package it.uniroma3.progettosiw.services;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.progettosiw.model.Album;
import it.uniroma3.progettosiw.model.Fotografia;
import it.uniroma3.progettosiw.repository.FotografiaRepository;

@Service
public class FotografiaService {

	@Autowired
	private FotografiaRepository fotografiaRepository;
	
	@Transactional
	public Fotografia inserisci(MultipartFile imageFile) throws Exception
	{
		String folder = "/photo/";
		byte[] bytes = imageFile.getBytes();
		Path path = Paths.get(folder + imageFile.getOriginalFilename());
		Files.write(path, bytes);
		return this.fotografiaRepository.save(new Fotografia(imageFile.getOriginalFilename(), bytes));
	}
	
	@Transactional
	public List<Fotografia> fotoPerAlbum(Album album)
	{
		return this.fotografiaRepository.findByAlbum(album);
	}

	@Transactional
	public List<Fotografia> fotoPerNome(String nome)
	{
		return this.fotografiaRepository.findByName(nome);
	}
	
	@Transactional
	public Fotografia fotoPerId(Long id)
	{
		return this.fotografiaRepository.findById(id).get();
	}

	public List<Fotografia> tutti() {
		return (List<Fotografia>)this.fotografiaRepository.findAll();
	}
	
}
