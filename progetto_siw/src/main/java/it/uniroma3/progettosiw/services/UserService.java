package it.uniroma3.progettosiw.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.progettosiw.model.User;
import it.uniroma3.progettosiw.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User inserisci(User user)
	{
		return this.userRepository.save(user);
	}
	
	@Transactional
	public User userPerId(Long id)
	{
		return this.userRepository.findById(id).get();
	}
	
	@Transactional
	public List<User> userPerNome(String nome)
	{
		return this.userRepository.findByFirstName(nome);
	}
}
