package it.uniroma3.progettosiw.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.model.Album;
import it.uniroma3.progettosiw.model.Fotografia;
import it.uniroma3.progettosiw.model.Fotografo;
import it.uniroma3.progettosiw.services.AlbumService;
import it.uniroma3.progettosiw.services.FotografoService;



/**
 * The MainController is a Spring Boot Controller to handle
 * the generic interactions with the home pages, and that do not refer to specific entities
 */
@Controller
public class MainController {
	
	@Autowired
	AlbumService albumService;
	
	@Autowired
	FotografoService fotografoService;
	
    public MainController() {
        super();
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/" or "/index".
     * This method prepares and dispatches the home view.
     *
     * @return the name of the home view
     */
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        return "home";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/welcome".
     * This method prepares and dispatches the welcome view.
     *
     * @return the name of the welcome view
     */
    @RequestMapping(value = { "/welcome" }, method = RequestMethod.GET)
    public String welcome(Model model) {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();     // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role);

        return "welcome";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/admin".
     * This method prepares and dispatches the admin view.
     *
     * @return the name of the admin view
     */
    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();    // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role);

        return "admin";
    }
    

    @RequestMapping(value = {"/addAlbum/{idFotografo}"}, method=RequestMethod.GET)
    public String addAlbum(@PathVariable("idFotografo") Long id, Model model)
    {
    	Fotografo fotografo = this.fotografoService.fotografoPerId(id);
    	Album album = new Album();
    	model.addAttribute("album", album);
    	model.addAttribute("fotografo", fotografo);
    	return "newAlbumForm.html";
    }
    
    @RequestMapping(value = {"/addFoto"})
    public String addPhoto(Model model)
    {
    	model.addAttribute("foto", new Fotografia());
    	return "newPhotoForm.html";
    }
    
    
    @RequestMapping(value = {"/addPhotographer"})
    public String addPhotogrpher(Model model)
    {
    	model.addAttribute("fotografo", new Fotografo());
    	return "newPhotographerForm.html";
    }
}