package it.uniroma3.progettosiw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.model.Album;
import it.uniroma3.progettosiw.model.Fotografo;
import it.uniroma3.progettosiw.services.AlbumService;
import it.uniroma3.progettosiw.services.FotografoService;


@Controller
public class AlbumController {

	
	@Autowired
	AlbumValidator albumValidator;
	
	@Autowired
	AlbumService albumService;
	
	@Autowired
	FotografoService fotografoService;

	@RequestMapping(value="/album/{idFotografo}", method=RequestMethod.POST)
	public String newAlbum(@PathVariable("idFotografo") Long id, 
			@Valid @ModelAttribute("album") Album album,
			Model model, BindingResult bindingResult) {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String role = details.getAuthorities().iterator().next().getAuthority();
		Fotografo fotografo = this.fotografoService.fotografoPerId(id);
		this.albumValidator.validate(album, bindingResult);
		if(!bindingResult.hasErrors()) {
			album.setFotografo(fotografo);
	    	fotografo.addAlbum(album);
	    	this.fotografoService.aggiorna(fotografo);
			this.albumService.inserisci(album);
			model.addAttribute("album", album);
			model.addAttribute("fotografo", fotografo);
			model.addAttribute("role", role);
			return "album.html";
		}
		else {
			model.addAttribute("fotografo", fotografo);
			return "newAlbumForm.html";
		}
	}
	
	@RequestMapping(value="/album/{id}", method=RequestMethod.GET)
	public String getAlbum(@PathVariable("id") Long id, Model model) {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String role = details.getAuthorities().iterator().next().getAuthority();
		Album album = this.albumService.albumPerId(id);
		model.addAttribute("album", album);
		model.addAttribute("fotografo", album.getFotografo());
		model.addAttribute("role", role);
		return "album.html";
	}
}
