package it.uniroma3.progettosiw.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import it.uniroma3.progettosiw.model.Fotografia;
import it.uniroma3.progettosiw.services.FotografiaService;

@Controller
public class FotografiaController {
	
	@Autowired
	FotografiaService fotografiaService;
	
	@PostMapping("/foto")
	public String fileUpload(@RequestParam("imageFile")MultipartFile imageFile, Model model, BindingResult bindingResult) {
		try {
			Fotografia foto = this.fotografiaService.inserisci(imageFile);
			model.addAttribute("album", foto.getAlbum());
			return "album.html";
		} catch (Exception e) {
			e.printStackTrace();
			return "newPhotoForm.html";
		}
	}

}
