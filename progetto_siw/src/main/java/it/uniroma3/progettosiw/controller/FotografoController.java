package it.uniroma3.progettosiw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.model.Fotografo;
import it.uniroma3.progettosiw.services.FotografoService;

@Controller
public class FotografoController {

	@Autowired
	FotografoValidator fotografoValidator;
	
	@Autowired
	FotografoService fotografoService;

	@RequestMapping(value="/fotografo", method=RequestMethod.POST)
	public String newPhotographer(@Valid @ModelAttribute("fotografo") Fotografo fotografo,
			Model model, BindingResult bindingResult) {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String role = details.getAuthorities().iterator().next().getAuthority();
		this.fotografoValidator.validate(fotografo, bindingResult);
		if(!bindingResult.hasErrors()) {
			this.fotografoService.inserisci(fotografo);
			model.addAttribute("fotografo", fotografo);
			model.addAttribute("albums", fotografo.getAlbum());
			model.addAttribute("role", role);
			return "fotografo.html";
		}
		else {
			return "newPhotographerForm.html";
		}
	}
	
	@RequestMapping(value="/fotografo/{id}", method=RequestMethod.GET)
	public String getFotografo(@PathVariable("id") Long id,
			Model model) {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String role = details.getAuthorities().iterator().next().getAuthority();
		Fotografo fotografo = this.fotografoService.fotografoPerId(id);
		model.addAttribute("fotografo", fotografo);
		model.addAttribute("albums", fotografo.getAlbum());
		model.addAttribute("role", role);
		return "fotografo.html";
	}
	
	@RequestMapping(value="/fotografi", method=RequestMethod.GET)
	public String getFotografi(Model model) {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String role = details.getAuthorities().iterator().next().getAuthority();
		model.addAttribute("fotografi", this.fotografoService.tutti());
		model.addAttribute("role", role);
		return "fotografi.html";
	}
}