package it.uniroma3.progettosiw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.progettosiw.model.Fotografo;


@Repository
public interface FotografoRepository extends CrudRepository<Fotografo, Long> {

	public List<Fotografo> findByFirstName(String nome);
	public List<Fotografo> findByFirstNameAndLastName(String nome, String cognome);
}
