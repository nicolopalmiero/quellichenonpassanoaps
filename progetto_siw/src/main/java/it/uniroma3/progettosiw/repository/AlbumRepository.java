package it.uniroma3.progettosiw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.progettosiw.model.Album;

@Repository
public interface AlbumRepository extends CrudRepository<Album, Long> {
	
	public List<Album> findByName(String nome);

}
