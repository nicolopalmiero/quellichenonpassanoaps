package it.uniroma3.progettosiw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.progettosiw.model.Album;
import it.uniroma3.progettosiw.model.Fotografia;

@Repository
public interface FotografiaRepository extends CrudRepository<Fotografia, Long> {

	public List<Fotografia> findByName(String nome);
	public List<Fotografia> findByAlbum(Album album);
}
