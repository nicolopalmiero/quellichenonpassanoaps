package it.uniroma3.progettosiw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.progettosiw.model.User;



@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	public List<User> findByFirstName(String nome);
	public List<User> findByFirstNameAndLastName(String nome, String cognome);
}
