package it.uniroma3.progettosiw.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class Fotografo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String firstName;
	private String lastName;
	@OneToMany(mappedBy="fotografo")
	private List<Album> album;
	@OneToMany(mappedBy="fotografo", cascade = CascadeType.PERSIST)
	@Autowired
	private Map <Long,Fotografia> fotografia;
	
	public Fotografo() {
		this.album = new LinkedList<Album>();
	}
	
	public Fotografo(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.album = new LinkedList<Album>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Album> getAlbum() {
		return album;
	}

	public void setAlbum(List<Album> album) {
		this.album = album;
	}

	public Map<Long, Fotografia> getFotografia() {
		return fotografia;
	}

	public void setFotografia(Map<Long, Fotografia> fotografia) {
		this.fotografia = fotografia;
	}

	public void addAlbum(Album album) {
		this.album.add(album);
	}	
	
	
}
