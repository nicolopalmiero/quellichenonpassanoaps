package it.uniroma3.progettosiw.model;

import javax.persistence.*;

/**
 * A User is a generic person who can use our application.
 * Subclasses of User include Admin and Customer.
 * @see User
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "last_name")
    protected String lastName;

    @Column(name = "username", unique=true)
    protected String username;

    @Column(name = "password")
    protected String password;

    @Column(name = "role")
    protected String role;

    /**
     * Constructor
     *
     * @param id The id of this User
     * @param firstName The first name of this User
     * @param lastName The last name of this User
     * @param username The username of this User for authentication
     * @param password The password of this User for authentication
     * @param role The authorization role for this User
     */
    public User(String firstName, String lastName, String username, String password, String role) {
    	this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.role = role;
    }

     public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }
   
    public void setRole(String role) {
        this.role = role;
    }
}
