package it.uniroma3.progettosiw.model;

import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Album {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Fotografo fotografo;
	@OneToMany(mappedBy="album", cascade = CascadeType.ALL)
	private Map<Long, Fotografia> fotografie;
	
	//constructors
	public Album() {}
	
	public Album(Fotografo fotografo) {
		this.fotografo = fotografo;
	}
	
	public Album(String name, Fotografo fotografo) {
		this.name = name;
		this.fotografo = fotografo;
	}
	
	//getters and setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Fotografo getFotografo() {
		return fotografo;
	}
	public void setFotografo(Fotografo fotografo) {
		this.fotografo = fotografo;
	}
	
	
}
